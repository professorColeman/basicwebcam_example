#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    cam.setup(1280, 720); //setup our webcam
    frame.allocate(1280, 720, OF_IMAGE_COLOR);  //setup our holder for each frame from the camera
}

//--------------------------------------------------------------
void ofApp::update(){
    if(cam.isInitialized()){ //make sure camera is ready
        cam.update(); //get a new frame from the camera
        if(cam.isFrameNew()){ //make sure we have a new camera
            frame.setFromPixels(cam.getPixels()); //copy the camera info to our frame holder
        }
        
    }
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    //cam.draw(0, 0); //draw the image from the camera
    //frame.draw(0,0); //draw the frame holder
    
    for(int x = 0; x<1280; x+=15){ //go thru the entire frame, 15 pixels at a time
        for(int y = 0; y<720; y+=15){
            ofColor pixel = frame.getColor(x, y); //grab the color of the pixel at this location
            pixel.setSaturation(220);  //pump up the saturation!
            pixel.setHue(pixel.getHue()-50); //shift the hue
            ofSetColor(pixel);
            float bright = pixel.getBrightness(); //calculate the brightness of the pixel
            //ofSetColor(bright);
            ofNoFill();
            ofDrawEllipse(x, y, bright/2, bright/2); //draw the ellipses, brighter ones are bigger
        }
    }
    
    
}

